(* 9. *)
(* 10. *)

type article
(** type representant un article avec un nom et un prix *)

type panier
(** type representant un panier contenant des articles *)

val creer_article : string -> float -> article
(** [creer_article nom prix] cree un article avec le nom [nom] et le prix
    [prix]
    @raise Invalid_argument si [nom] est vide ou [prix] est negatif ou nul *)
(* constructeur en O(1) *)

val prix_article : article -> float
(** [prix_article article] retourne le prix de l'article [article] *)
(* accesseur en O(1) *)

val nom_article : article -> string
(** [nom_article article] retourne le nom de l'article [article] *)
(* accesseur en O(1) *)

val panier_vide : unit -> panier
(** [panier_vide ()] retourne un panier vide *)
(* constructeur en O(1) *)

val est_vide : panier -> bool
(** [est_vide panier] retourne [true] si le panier [panier] est vide, [false]
    sinon *)
(* accesseur en O(1) *)

val depose_article : panier -> article -> panier
(** [depose_article panier article] retourne un nouveau panier contenant
    l'article [article] ajoute au panier [panier] *)
(* transformateur en O(1) *)

val dernier_article : panier -> article
(** [dernier_article panier] retourne le dernier article ajoute au panier
    [panier]
    @raise End_of_file si le panier est vide *)
(* accesseur en O(1) *)

val retire_article : panier -> panier
(** [retire_article panier] retourne un nouveau panier contenant tous les
    articles du panier [panier] sauf le dernier
    @raise End_of_file si le panier est vide *)
(* transformateur en O(1) *)

val nombre_articles : panier -> int
(** [nombre_articles panier] retourne le nombre d'articles dans le panier
    [panier] *)
(* accesseur en O(n) *)

val prix_panier : panier -> float
(** [prix_panier panier] retourne le prix total des articles dans le panier
    [panier] *)
(* accesseur en O(n) *)
