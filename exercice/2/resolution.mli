(* 12. *)
val depuis_ficher : string -> Panier.panier

(* 13. *)
val est_solution : Panier.panier -> float -> int -> bool

(* 14. *)
val resolution_naive : Panier.panier -> float -> int option

(* 16. *)
val enregiste_solution : Panier.panier -> float -> string -> unit

(* 17. *)
val partition : Panier.panier -> Panier.panier * Panier.panier

(* 18. *)
val insere : float list -> float -> float list

(* 19. *)
val toutes_sommes : Panier.panier -> float list

(* 20. *)
val renverse : float list -> float list

(* 21. *)
val resolution_rencontre_milieu : Panier.panier -> float -> bool

(* 22. *)
val resolution_programmation_dynamique : Panier.panier -> float -> bool

(* 23. *)
val resolution_dynamique : Panier.panier -> float -> int option
val enregiste_solution_dynamique : Panier.panier -> float -> string -> unit
