(* 8. *)

(* 1. *)
type article = { nom : string; prix : float }
type panier = article list

(* 2. *)
let creer_article nom prix =
  if nom = "" then raise (Invalid_argument "nom vide")
    (* Ou assert (nom <> "") si preferable pour la programmation defensive *)
  else if prix <= 0. then raise (Invalid_argument "prix negatif ou nul")
    (* Ou assert (prix > 0.) si preferable pour la programmation defensive *)
  else { nom; prix }

let prix_article article = article.prix
let nom_article article = article.nom

(* 3. *)
let panier_vide () = []
let est_vide panier = panier = []

(* 4. *)
let depose_article panier article = article :: panier

(* 5. *)
let dernier_article = function
  | article :: _ -> article
  | [] -> raise End_of_file
(* Ou assert false si preferable pour la programmation defensive *)

let retire_article = function _ :: panier -> panier | [] -> raise End_of_file
(* Ou assert false si preferable pour la programmation defensive *)

(* 6. *)
let rec nombre_articles = function
  | _ :: panier -> 1 + nombre_articles panier
  | [] -> 0

(* 7. *)
let rec prix_panier = function
  | article :: panier -> article.prix +. prix_panier panier
  | [] -> 0.
