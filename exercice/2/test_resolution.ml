let egaux f1 f2 = abs_float (f1 -. f2) < Float.epsilon *. 64.

let creer_panier () =
  Panier.depose_article
    (Panier.depose_article
       (Panier.depose_article
          (Panier.depose_article (Panier.panier_vide ())
             (Panier.creer_article "tasse" 15.))
          (Panier.creer_article "bracelet" 7.4))
       (Panier.creer_article "clavier" 22.6))
    (Panier.creer_article "crayon" 1.2)

let test_depuis_ficher () =
  assert (Resolution.depuis_ficher "" = Panier.panier_vide ());
  assert (Resolution.depuis_ficher "panier.txt" = creer_panier ())

let test_est_solution () =
  let p = Panier.panier_vide () in
  assert (Resolution.est_solution p 0. 0);
  assert (not (Resolution.est_solution p 1. 0));
  let p = creer_panier () in
  assert (Resolution.est_solution p 23.6 0b1101)

let test_solution f =
  let p = Panier.panier_vide () in
  assert (f p 0. = Some 0);
  assert (f p 1. = None);
  let p = creer_panier () in
  assert (f p 0. = Some 0);
  let s = Panier.prix_panier p in
  assert (f p (s +. 1.) = None);
  assert (f p s = Some ((1 lsl Panier.nombre_articles p) - 1));
  match f p 23.6 with
  | Some s -> assert (Resolution.est_solution p 23.6 s)
  | None -> assert false

let test_resolution_naive () = test_solution Resolution.resolution_naive

let rec read_in i =
  match try Some (input_line i) with End_of_file -> None with
  | Some s -> s :: read_in i
  | None -> []

let read_file n =
  let f = open_in n in
  let r = read_in f in
  close_in f;
  r

let test_enregiste f =
  let p = Panier.panier_vide () in
  f p 0. "solution.txt";
  assert (read_file "solution.txt" = []);
  f p 1. "solution.txt";
  assert (read_file "solution.txt" = []);
  let p = creer_panier () in
  f p 0. "solution.txt";
  assert (read_file "solution.txt" = []);
  f p (Panier.prix_panier p) "solution.txt";
  assert (
    read_file "solution.txt" = [ "crayon"; "clavier"; "bracelet"; "tasse" ])

let test_enregiste_solution () = test_enregiste Resolution.enregiste_solution

let test_partition () =
  let p = Panier.panier_vide () in
  assert (Resolution.partition p = (p, p));
  let a = Panier.creer_article "rien" 1. in
  let q = Panier.depose_article p a in
  assert (Resolution.partition q = (q, p));
  let p = Panier.depose_article q a in
  assert (Resolution.partition p = (q, q));
  assert (Resolution.partition (Panier.depose_article p a) = (p, q))

let test_insere () =
  assert (Resolution.insere [] 3. = [ 3. ]);
  assert (Resolution.insere [ 1. ] 3. = [ 1.; 3. ]);
  assert (Resolution.insere [ 3. ] 1. = [ 1.; 3. ]);
  assert (Resolution.insere [ 1.; 2.; 4. ] 3. = [ 1.; 2.; 3.; 4. ]);
  assert (Resolution.insere [ 1.; 2.; 3. ] 3. = [ 1.; 2.; 3.; 3. ]);
  assert (Resolution.insere [ 1.; 2.; 3. ] 2. = [ 1.; 2.; 2.; 3. ])

let rec print_list = function
  | [] -> ()
  | e :: l ->
      print_float e;
      print_string " ";
      print_list l

let rec egaux_liste l1 l2 =
  match (l1, l2) with
  | x :: l1, y :: l2 -> egaux x y && egaux_liste l1 l2
  | l1, l2 -> l1 = l2

let test_toutes_sommes () =
  let p = Panier.panier_vide () in
  assert (Resolution.toutes_sommes p = [ 0. ]);
  assert (
    Resolution.toutes_sommes
      (Panier.depose_article p (Panier.creer_article "rien" 1.))
    = [ 0.; 1. ]);
  assert (
    egaux_liste
      (Resolution.toutes_sommes (creer_panier ()))
      [
        0.;
        1.2;
        7.4;
        8.6;
        15.;
        16.2;
        22.4;
        22.6;
        23.6;
        23.8;
        30.;
        31.2;
        37.6;
        38.8;
        45.;
        46.2;
      ])

let test_renverse () =
  assert (Resolution.renverse [] = []);
  assert (Resolution.renverse [ 1. ] = [ 1. ]);
  assert (Resolution.renverse [ 1.; 2. ] = [ 2.; 1. ]);
  assert (Resolution.renverse [ 1.; 2.; 3. ] = [ 3.; 2.; 1. ])

let test_resolution f =
  let p = Panier.panier_vide () in
  assert (f p 0.);
  assert (not (f p 1.));
  let p = creer_panier () in
  assert (f p 0.);
  let s = Panier.prix_panier p in
  assert (not (f p (s +. 1.)));
  assert (f p s);
  assert (f p 23.6)

let test_resolution_rencontre_milieu () =
  test_resolution Resolution.resolution_rencontre_milieu

let test_resolution_programmation_dynamique () =
  test_resolution Resolution.resolution_programmation_dynamique

let test_resolution_dynamique () = test_solution Resolution.resolution_dynamique

let test_enregiste_solution_dynamique () =
  test_enregiste Resolution.enregiste_solution_dynamique

let _ =
  test_depuis_ficher ();
  test_est_solution ();
  test_resolution_naive ();
  test_enregiste_solution ();
  test_partition ();
  test_insere ();
  test_toutes_sommes ();
  test_renverse ();
  test_resolution_rencontre_milieu ();
  test_resolution_programmation_dynamique ();
  test_resolution_dynamique ();
  test_enregiste_solution_dynamique ()
