let test_creer_article () =
  let a = Panier.creer_article "pomme" 1. in
  assert (Panier.nom_article a = "pomme");
  assert (Panier.prix_article a = 1.);
  assert (
    try
      let _ = Panier.creer_article "banane" 0. in
      false
    with Invalid_argument _ -> true);
  assert (
    try
      let _ = Panier.creer_article "fraise" (-1.) in
      false
    with Invalid_argument _ -> true);
  assert (
    try
      let _ = Panier.creer_article "" 1. in
      false
    with Invalid_argument _ -> true)

let test_prix_article () =
  assert (Panier.prix_article (Panier.creer_article "pomme" 1.) = 1.);
  assert (
    Panier.prix_article
      (Panier.creer_article "banane"
         3.141592653589793238462643383279502884197169399375105820974944592307816406286208998628034825342117068)
    = 3.141592653589793238462643383279502884197169399375105820974944592307816406286208998628034825342117068)

let test_nom_article () =
  assert (Panier.nom_article (Panier.creer_article "pomme" 1.) = "pomme");
  assert (
    Panier.nom_article
      (Panier.creer_article "banane"
         3.141592653589793238462643383279502884197169399375105820974944592307816406286208998628034825342117068)
    = "banane")

let test_panier_vide () = assert (Panier.est_vide (Panier.panier_vide ()))

let test_est_vide () =
  let vide = Panier.panier_vide () in
  assert (Panier.est_vide vide);
  let non_vide = Panier.depose_article vide (Panier.creer_article "pomme" 1.) in
  assert (not (Panier.est_vide non_vide))

let test_depose_article () =
  let p =
    Panier.depose_article (Panier.panier_vide ())
      (Panier.creer_article "pomme" 1.)
  in
  assert (not (Panier.est_vide p));
  let a = Panier.creer_article "banane" 42. in
  assert (Panier.dernier_article (Panier.depose_article p a) = a)

let test_dernier_article () =
  let p = Panier.panier_vide () in
  assert (
    try
      let _ = Panier.dernier_article p in
      false
    with End_of_file -> true);
  let a = Panier.creer_article "banane" 42. in
  let p = Panier.depose_article p a in
  assert (Panier.dernier_article p = a);
  let a = Panier.creer_article "pomme" 1. in
  assert (Panier.dernier_article (Panier.depose_article p a) = a)

let test_retire_article () =
  let p = Panier.panier_vide () in
  assert (
    try
      let _ = Panier.retire_article p in
      false
    with End_of_file -> true);
  let a = Panier.creer_article "pomme" 100000. in
  let p =
    Panier.retire_article
      (Panier.depose_article
         (Panier.depose_article p a)
         (Panier.creer_article "banane" 0.0000001))
  in
  assert (Panier.dernier_article p = a);
  assert (Panier.est_vide (Panier.retire_article p))

let test_nombre_articles () =
  let p = Panier.panier_vide () in
  assert (Panier.nombre_articles p = 0);
  let p = Panier.depose_article p (Panier.creer_article "pomme" 1.) in
  assert (Panier.nombre_articles p = 1);
  let p = Panier.depose_article p (Panier.creer_article "banane" 2.) in
  assert (Panier.nombre_articles p = 2);
  let a = Panier.creer_article "fraise" 3. in
  let p = Panier.depose_article (Panier.depose_article p a) a in
  assert (Panier.nombre_articles p = 4);
  let p = Panier.retire_article p in
  assert (Panier.nombre_articles p = 3);
  let p = Panier.retire_article (Panier.retire_article p) in
  assert (Panier.nombre_articles p = 1);
  assert (Panier.nombre_articles (Panier.retire_article p) = 0)

let test_prix_panier () =
  let p = Panier.panier_vide () in
  assert (Panier.prix_panier p = 0.);
  let p = Panier.depose_article p (Panier.creer_article "pomme" 1.) in
  assert (Panier.prix_panier p = 1.);
  let p = Panier.depose_article p (Panier.creer_article "banane" 2.) in
  assert (Panier.prix_panier p = 3.);
  let a = Panier.creer_article "fraise" 3. in
  let p = Panier.depose_article (Panier.depose_article p a) a in
  assert (Panier.prix_panier p = 9.);
  let p = Panier.retire_article p in
  assert (Panier.prix_panier p = 6.);
  let p = Panier.retire_article (Panier.retire_article p) in
  assert (Panier.prix_panier p = 1.);
  assert (Panier.prix_panier (Panier.retire_article p) = 0.)

let _ =
  test_creer_article ();
  test_prix_article ();
  test_nom_article ();
  test_panier_vide ();
  test_est_vide ();
  test_depose_article ();
  test_dernier_article ();
  test_retire_article ();
  test_nombre_articles ();
  test_prix_panier ()
