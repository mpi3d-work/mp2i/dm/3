let egaux f1 f2 = abs_float (f1 -. f2) < Float.epsilon *. 16.

(* 12. *)
exception Break of int

(* fonction qui renvoie un couple de chaînes de caractères, la première
   jusqu'au premier espace, la seconde après *)
let split s =
  let l = String.length s - 1 in
  try
    for i = 0 to l do
      if s.[i] = ' ' then raise (Break i)
    done;
    raise (Invalid_argument "pas d'espace dans la chaine")
  with Break i -> (String.sub s 0 i, String.sub s (i + 1) (l - i))

(* fonction qui lit un panier depuis un canal d'entrée *)
let rec depuis_in i =
  match try Some (input_line i) with End_of_file -> None with
  | Some s ->
      let p = depuis_in i in
      if s = "" then p (* On ignore les lignes vides *)
      else
        let v, n = split s in
        Panier.depose_article p (Panier.creer_article n (float_of_string v))
  | None -> Panier.panier_vide ()

let depuis_ficher n =
  match
    try Some (open_in n)
    with Sys_error s ->
      output_string stderr ("Erreur d'ouverture de fichier: " ^ s);
      None
  with
  | Some i ->
      let p = depuis_in i in
      close_in i;
      p
  | None -> Panier.panier_vide ()

(* 13. *)
let rec est_solution p b = function
  | 0 -> egaux b 0.
  | s ->
      est_solution (Panier.retire_article p)
        (if s land 1 = 1 then
           b -. Panier.prix_article (Panier.dernier_article p)
         else b)
        (s lsr 1)

(* 14. *)
let rec resolution_naive p b =
  (* si le budget correspond exactement à la somme des prix des articles *)
  if egaux b 0. then Some 0
    (* si le budget est negatif il n'y a pas de solution *)
    (* si le panier est vide et que le budget n'est pas attends il n'y a pas de
       solution non plus *)
  else if b < 0. || Panier.est_vide p then None
  else
    let q = Panier.retire_article p in
    (* on teste sans l'article *)
    match resolution_naive q b with
    | None -> (
        match
          (* on teste avec l'article *)
          resolution_naive q
            (b -. Panier.prix_article (Panier.dernier_article p))
        with
        | None -> None
        | Some s -> Some ((s lsl 1) lor 1))
    | Some s -> Some (s lsl 1)

(* 15. *)
(* si on note n le nombre d'articles dans le panier, il y a 2^n solutions
   potentielles donc la complexite de resolution_naive est de O(2^n) *)

(* 16. *)
(* fonction qui écrit un panier dans un canal de sortie *)
let rec ecrire o p = function
  | 0 -> ()
  | s ->
      if s land 1 = 1 then
        output_string o (Panier.nom_article (Panier.dernier_article p) ^ "\n");
      ecrire o (Panier.retire_article p) (s lsr 1)

(* fonction qui enregistre une solution dans un fichier *)
let enregiste s p b n =
  let f = open_out n in
  (match s p b with Some s -> ecrire f p s | None -> ());
  close_out f

let enregiste_solution = enregiste resolution_naive

(* 17. *)
let rec partition p =
  if Panier.est_vide p then (* ([], []) *)
    (p, p)
  else
    let q = Panier.retire_article p in
    if Panier.est_vide q then (* (p, []) *)
      (p, q)
    else
      let a, b = partition (Panier.retire_article q) in
      (* (a :: p, b :: q) *)
      ( Panier.depose_article a (Panier.dernier_article p),
        Panier.depose_article b (Panier.dernier_article q) )

(* 18. *)
let rec insere l f =
  match l with
  | x :: t -> if f > x then x :: insere t f else f :: l
  | [] -> [ f ]

(* 19. *)
(* base sur le tri par insertion *)
let rec toutes_sommes_v1 =
  let rec somme c s p =
    if Panier.est_vide p then insere c s
    else
      let q = Panier.retire_article p in
      somme
        (somme c (s +. Panier.prix_article (Panier.dernier_article p)) q)
        s q
  in
  somme [] 0.

(* fonction qui fusionne deux listes triées *)
let rec fusion l1 l2 =
  match (l1, l2) with
  | x :: m1, y :: m2 -> if x < y then x :: fusion m1 l2 else y :: fusion l1 m2
  | [], l -> l
  | l, [] -> l

(* fonction qui ajoute i à tous les éléments d'une liste *)
let rec incremente i = function x :: l -> (x +. i) :: incremente i l | l -> l

(* base sur le tri fusion *)
let rec toutes_sommes_v2 p =
  if Panier.est_vide p then [ 0. ]
  else
    let l = toutes_sommes_v2 (Panier.retire_article p) in
    fusion l (incremente (Panier.prix_article (Panier.dernier_article p)) l)

let toutes_sommes = toutes_sommes_v2

(* 20. *)
(* recusivite terminale *)
let renverse =
  let rec rev c = function x :: l -> rev (x :: c) l | [] -> c in
  rev []

(* 21. *)
let rec rencontre_milieu b l1 l2 =
  match (l1, l2) with
  | x :: m1, y :: m2 ->
      let s = x +. y in
      egaux s b
      ||
      if s < b then
        (* s < b donc il faut augmente la somme *)
        rencontre_milieu b m1 l2
      else (* s > b donc il faut diminue la somme *)
        rencontre_milieu b l1 m2
  | _ -> false

let resolution_rencontre_milieu p b =
  let p, q = partition p in
  rencontre_milieu b (toutes_sommes p) (renverse (toutes_sommes q))

(* 22. *)
let resolution_dynamique =
  let a = Hashtbl.create 0 in
  let rec resolution p b =
    match Hashtbl.find_opt a (p, b) with
    (* on connait deja la solution *)
    | Some s -> s
    (* on ne connait pas la solution *)
    | None ->
        let s =
          if egaux b 0. then Some 0
            (* si le budget est negatif il n'y a pas de solution *)
            (* si le panier est vide et que le budget n'est pas attends il n'y a pas de
               solution non plus *)
          else if b < 0. || Panier.est_vide p then None
          else
            let q = Panier.retire_article p in
            (* on teste sans l'article *)
            match resolution q b with
            | None -> (
                match
                  (* on teste avec l'article *)
                  resolution q
                    (b -. Panier.prix_article (Panier.dernier_article p))
                with
                | None -> None
                | Some s -> Some ((s lsl 1) lor 1))
            | Some s -> Some (s lsl 1)
        in
        (* on enregistre la solution *)
        Hashtbl.add a (p, b) s;
        s
  in
  resolution

let resolution_programmation_dynamique p b = resolution_dynamique p b <> None

(* 23. *)
let enregiste_solution_dynamique = enregiste resolution_dynamique
