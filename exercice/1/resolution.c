#include <assert.h>
#include <limits.h>
#include <stdlib.h>

#include "liste.h"
#include "resolution.h"

// 1.

// Retourne le maximum de la liste l et enleve le maximum de la liste l
int maximum(liste *l)
{
    liste a = *l;
    assert(!est_vide_liste(a));

    // Une iteration sur la liste
    // Maximum
    int m = tete_liste(a);
    queue_liste(a);

    // Liste de retour
    liste b = creer_liste();
    while (!est_vide_liste(a))
    {
        // Une iteration sur la liste
        int e = tete_liste(a);
        queue_liste(a);

        if (e > m)
        {
            // On echange m et e
            int t = m;
            m = e;
            e = t;
        }

        ajouter_liste(b, e);
    }

    detruire_liste(a);
    *l = b; // On remplace la liste par la liste b

    return m; // On retourne le maximum
}

// Liste l est detruite
int resolution_simple(liste l)
{
    maximum(&l);         // Premier maximum
    int m = maximum(&l); // Deuxieme maximum

    detruire_liste(l);

    return m;
}

// 2.

// Complexite C(n) = 2n-3 comparaison car le premier appel a maximum fait n-1
// comparaisons et donne une liste de taille n-1, le deuxieme appel a maximum
// fait n-2 comparaisons et donne une liste de taille n-2. On a donc
// C(n) = (n-1) + (n-2) = 2n-3 comparaisons

// 3.

// Partage la liste l en deux listes l1 et l2 avec card(l1) >= card(l2)
// Liste l est detruite
void partage(liste l, liste *l1, liste *l2)
{
    liste a = creer_liste();
    liste b = creer_liste();

    while (!est_vide_liste(l))
    {
        // Une iteration sur la liste
        ajouter_liste(a, tete_liste(l));
        queue_liste(l);

        // On echange l1 et l2
        liste t = a;
        a = b;
        b = t;
    }

    detruire_liste(l);

    *l1 = b;
    *l2 = a;
}

// 4.

// Detruit la liste l
void regner(liste l, int *max_1, int *max_2)
{
    liste l1, l2;
    partage(l, &l1, &l2);

    // Si l1 est vide alors l2 est vide
    if (est_vide_liste(l2))
    {
        // card(l) <= 1
        if (!est_vide_liste(l1))
        {
            // card(l) = 1
            int e = tete_liste(l1);
            if (e > *max_1)
            {
                // On decale max_1 et max_2 avec e
                *max_2 = *max_1;
                *max_1 = e;
            }
            else if (e > *max_2)
                *max_2 = e;
        }

        detruire_liste(l1);
        detruire_liste(l2);
    }
    else
    {
        // card(l) > 1
        regner(l1, max_1, max_2);
        regner(l2, max_1, max_2);
    }
}

// Detruit la liste l
int resolution_diviser_pour_regner(liste l)
{
    int max_1 = INT_MIN;
    int max_2 = INT_MIN;

    regner(l, &max_1, &max_2);

    return max_2;
}

// 5.

// C(0) = 0
// C(2^0) = C(1) = 2
// C(2^1) = C(2) = 2 * C(2^0) = 4
// C(2^2) = C(4) = 2 * C(2^1) = 8
// C(2^3) = C(8) = 2 * C(2^2) = 16
// ...
// C(2^n) = 2 * C(2^(n-1)) = 2^(n+1) comparaisons au maximum

// 6.

// C(0) = 0 + 0 = 0
// C(2^0) = C(1) = 0 + 0 = 0
// C(2^1) = C(2) = 1 + 1 = 2
// C(2^2) = C(4) = 3 + 2 = 5
// C(2^3) = C(8) = 7 + 3 = 10
// C(2^4) = C(16) = 15 + 4 = 19
// ...
// C(2^n) = 2^n - 1 + n comparaisons

// 7.

// Detruit la liste l
tournoi preparer_tournoi(liste l)
{
    tournoi t = NULL;

    while (!est_vide_liste(l))
    {
        // Une iteration sur la liste
        int e = tete_liste(l);
        queue_liste(l);

        tournoi s = malloc(sizeof(liste_couples));
        assert(s != NULL);

        // On initialise le tournoi
        s->vainqueur = e;
        s->vaincus = creer_liste();
        s->suivant = t;

        t = s;
    }

    detruire_liste(l);

    return t;
}

// 8.

// Detruit le tournoi t
void detruire_tournoi(tournoi t)
{
    while (t != NULL)
    {
        tournoi s = t->suivant;

        detruire_liste(t->vaincus);
        free(t);

        t = s;
    }
}

// 9.

// Partage le tournoi t en deux tournoi t1 et t2 avec card(t1) >= card(t2)
// Detruit le tournoi t
void partage_tournoi(tournoi t, tournoi *t1, tournoi *t2)
{
    tournoi a = NULL;
    tournoi b = NULL;

    while (t != NULL)
    {
        // On echange a, b, t et t->suivant
        tournoi s = t->suivant;
        t->suivant = a;
        a = b;
        b = t;
        t = s;
    }

    *t1 = b;
    *t2 = a;
}

// 10.

// Detruit le tournoi t1 et t2
// On suppose de plus que card(t1) >= card(t2)
tournoi un_tour(tournoi t1, tournoi t2)
{
    tournoi t = NULL;

    while (t2 != NULL) // si t1 est vide alors t2 est vide
    {
        tournoi s1 = t1->suivant;
        tournoi s2 = t2->suivant;

        if (t1->vainqueur < t2->vainqueur)
        {
            // On echange t1 et t2
            tournoi t3 = t1;
            t1 = t2;
            t2 = t3;
        }

        // t1 est le vainqueur et t2 est le vaincus

        detruire_liste(t2->vaincus);
        ajouter_liste(t1->vaincus, t2->vainqueur);
        free(t2);

        // On passe t au suivant
        t1->suivant = t;
        t = t1;

        // On restore les suivants
        t1 = s1;
        t2 = s2;
    }

    if (t1 != NULL)
    {
        t1->suivant = t;
        t = t1;
    }

    return t;
}

// 11.

// Detruit le tournoi t
tournoi tous_les_tours(tournoi t)
{
    while (t->suivant != NULL)
    {
        tournoi t1, t2;
        partage_tournoi(t, &t1, &t2);
        t = un_tour(t1, t2);
    }

    return t;
}

// 12.

// Detruit la liste l
int resolution_tournoi(liste l)
{
    tournoi t = preparer_tournoi(l);
    t = tous_les_tours(t);

    l = t->vaincus;

    // Maximum
    int m = INT_MIN;
    while (!est_vide_liste(l))
    {
        // Une iteration sur la liste
        int e = tete_liste(l);
        queue_liste(l);

        if (e > m)
            m = e;
    }

    detruire_liste(l);
    free(t);

    return m;
}
