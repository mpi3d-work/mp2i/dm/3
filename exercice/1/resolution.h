#include "liste.h"

// 1.
int resolution_simple(liste);

// 3.
void partage(liste, liste *, liste *);

// 4.
int resolution_diviser_pour_regner(liste);

struct liste_couples_s
{
    int vainqueur;
    liste vaincus;
    struct liste_couples_s *suivant;
};
typedef struct liste_couples_s liste_couples;
typedef liste_couples *tournoi;

// 7.
tournoi preparer_tournoi(liste);

// 8.
void detruire_tournoi(tournoi);

// 9.
void partage_tournoi(tournoi t, tournoi *, tournoi *);

// 10.
tournoi un_tour(tournoi, tournoi);

// 11.
tournoi tous_les_tours(tournoi);

// 12.
int resolution_tournoi(liste);
