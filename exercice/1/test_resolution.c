#include <assert.h>
#include <stddef.h>

#include "resolution.h"

liste creer_liste_0()
{
    liste l = creer_liste();

    ajouter_liste(l, 3);

    return l;
}

liste creer_liste_1()
{
    liste l = creer_liste();

    ajouter_liste(l, 9);
    ajouter_liste(l, 4);
    ajouter_liste(l, 5);
    ajouter_liste(l, 8);
    ajouter_liste(l, 6);
    ajouter_liste(l, 1);
    ajouter_liste(l, 3);

    return l;
}

liste creer_liste_2()
{
    liste l = creer_liste();

    ajouter_liste(l, 7);
    ajouter_liste(l, 2);

    return l;
}

liste creer_liste_3()
{
    liste l = creer_liste();

    ajouter_liste(l, 4);
    ajouter_liste(l, 1);
    ajouter_liste(l, 6);

    return l;
}

liste creer_liste_4(int n)
{

    liste l = creer_liste();

    for (int i = 1; i <= n; i += 1)
        ajouter_liste(l, i);

    return l;
}

void test_resolution_simple()
{
    assert(resolution_simple(creer_liste_1()) == 8);
    assert(resolution_simple(creer_liste_2()) == 2);
    assert(resolution_simple(creer_liste_3()) == 4);
    assert(resolution_simple(creer_liste_4(34)) == 33);
    assert(resolution_simple(creer_liste_4(1000)) == 999);
}

void test_partage()
{
    liste l1, l2;

    partage(creer_liste(), &l1, &l2);
    assert(est_vide_liste(l1));
    assert(est_vide_liste(l2));
    detruire_liste(l1);
    detruire_liste(l2);

    partage(creer_liste_0(), &l1, &l2);
    assert(tete_liste(l1) == 3);
    queue_liste(l1);
    assert(est_vide_liste(l1));
    assert(est_vide_liste(l2));
    detruire_liste(l1);
    detruire_liste(l2);

    partage(creer_liste_1(), &l1, &l2);
    assert(tete_liste(l1) == 9);
    queue_liste(l1);
    assert(tete_liste(l1) == 5);
    queue_liste(l1);
    assert(tete_liste(l2) == 4);
    queue_liste(l2);
    assert(tete_liste(l2) == 8);
    detruire_liste(l1);
    detruire_liste(l2);

    partage(creer_liste_2(), &l1, &l2);
    assert(tete_liste(l1) == 7);
    queue_liste(l1);
    assert(est_vide_liste(l1));
    assert(tete_liste(l2) == 2);
    queue_liste(l2);
    assert(est_vide_liste(l2));
    detruire_liste(l1);
    detruire_liste(l2);

    partage(creer_liste_3(), &l1, &l2);
    assert(tete_liste(l1) == 4);
    queue_liste(l1);
    assert(tete_liste(l1) == 6);
    queue_liste(l1);
    assert(est_vide_liste(l1));
    assert(tete_liste(l2) == 1);
    queue_liste(l2);
    assert(est_vide_liste(l2));
    detruire_liste(l1);
    detruire_liste(l2);
}

void test_diviser_pour_regner()
{
    assert(resolution_diviser_pour_regner(creer_liste_1()) == 8);
    assert(resolution_diviser_pour_regner(creer_liste_2()) == 2);
    assert(resolution_diviser_pour_regner(creer_liste_3()) == 4);
    assert(resolution_diviser_pour_regner(creer_liste_4(34)) == 33);
    assert(resolution_diviser_pour_regner(creer_liste_4(1000)) == 999);
}

void test_preparer_tournoi()
{
    tournoi t;
    t = preparer_tournoi(creer_liste());
    assert(t == NULL);
    detruire_tournoi(t);

    t = preparer_tournoi(creer_liste_0());
    assert(t->vainqueur == 3);
    assert(est_vide_liste(t->vaincus));
    assert(t->suivant == NULL);
    detruire_tournoi(t);

    t = preparer_tournoi(creer_liste_4(1000));
    tournoi d = t;
    for (int i = 1; i <= 1000; i += 1)
    {
        assert(t != NULL);
        assert(t->vainqueur == i);
        assert(est_vide_liste(t->vaincus));
        t = t->suivant;
    }
    assert(t == NULL);
    detruire_tournoi(d);
}

void test_detruire_tournoi()
{
    detruire_tournoi(preparer_tournoi(creer_liste()));
    detruire_tournoi(preparer_tournoi(creer_liste_0()));
    detruire_tournoi(preparer_tournoi(creer_liste_1()));
    detruire_tournoi(preparer_tournoi(creer_liste_2()));
    detruire_tournoi(preparer_tournoi(creer_liste_3()));
    detruire_tournoi(preparer_tournoi(creer_liste_4(34)));
    detruire_tournoi(preparer_tournoi(creer_liste_4(1000)));
}

void test_partage_tournoi()
{
    tournoi t1, t2;

    partage_tournoi(preparer_tournoi(creer_liste()), &t1, &t2);
    assert(t1 == NULL);
    assert(t2 == NULL);
    detruire_tournoi(t1);
    detruire_tournoi(t2);

    partage_tournoi(preparer_tournoi(creer_liste_0()), &t1, &t2);
    assert(t1->vainqueur == 3);
    assert(t1->suivant == NULL);
    assert(t2 == NULL);
    detruire_tournoi(t1);
    detruire_tournoi(t2);

    partage_tournoi(preparer_tournoi(creer_liste_1()), &t1, &t2);
    assert(t1->vainqueur == 3);
    assert(t1->suivant != NULL);
    assert(t1->suivant->vainqueur == 6);
    assert(t2->vainqueur == 1);
    assert(t2->suivant != NULL);
    assert(t2->suivant->vainqueur == 8);
    detruire_tournoi(t1);
    detruire_tournoi(t2);

    partage_tournoi(preparer_tournoi(creer_liste_2()), &t1, &t2);
    assert(t1->vainqueur == 2);
    assert(t1->suivant == NULL);
    assert(t2->vainqueur == 7);
    assert(t2->suivant == NULL);
    detruire_tournoi(t1);
    detruire_tournoi(t2);

    partage_tournoi(preparer_tournoi(creer_liste_3()), &t1, &t2);
    assert(t1->vainqueur == 6);
    assert(t1->suivant != NULL);
    assert(t1->suivant->vainqueur == 4);
    assert(t1->suivant->suivant == NULL);
    assert(t2->vainqueur == 1);
    assert(t2->suivant == NULL);
    detruire_tournoi(t1);
    detruire_tournoi(t2);
}

void test_un_tour()
{
    tournoi t1, t2;
    tournoi t;

    partage_tournoi(preparer_tournoi(creer_liste()), &t1, &t2);
    t = un_tour(t1, t2);
    assert(t == NULL);
    detruire_tournoi(t);

    partage_tournoi(preparer_tournoi(creer_liste_0()), &t1, &t2);
    t = un_tour(t1, t2);
    assert(t->vainqueur == 3);
    assert(est_vide_liste(t->vaincus));
    assert(t->suivant == NULL);
    detruire_tournoi(t);

    partage_tournoi(preparer_tournoi(creer_liste_2()), &t1, &t2);
    t = un_tour(t1, t2);
    assert(t->vainqueur == 7);
    assert(tete_liste(t->vaincus) == 2);
    queue_liste(t->vaincus);
    assert(est_vide_liste(t->vaincus));
    assert(t->suivant == NULL);
    detruire_tournoi(t);

    partage_tournoi(preparer_tournoi(creer_liste_3()), &t1, &t2);
    t = un_tour(t1, t2);
    assert(t->vainqueur == 4);
    assert(est_vide_liste(t->vaincus));
    assert(t->suivant != NULL);
    assert(t->suivant->vainqueur == 6);
    assert(tete_liste(t->suivant->vaincus) == 1);
    queue_liste(t->suivant->vaincus);
    assert(est_vide_liste(t->suivant->vaincus));
    assert(t->suivant->suivant == NULL);
    detruire_tournoi(t);
}

void test_tournoi()
{
    assert(resolution_tournoi(creer_liste_1()) == 8);
    assert(resolution_tournoi(creer_liste_2()) == 2);
    assert(resolution_tournoi(creer_liste_3()) == 4);
    assert(resolution_tournoi(creer_liste_4(34)) == 33);
    assert(resolution_tournoi(creer_liste_4(1000)) == 999);
}

void test_tous_les_tours()
{
    tournoi t = tous_les_tours(preparer_tournoi(creer_liste_0()));
    assert(t->vainqueur == 3);
    assert(est_vide_liste(t->vaincus));
    assert(t->suivant == NULL);
    detruire_tournoi(t);

    t = tous_les_tours(preparer_tournoi(creer_liste_1()));
    assert(t->vainqueur == 9);
    assert(t->suivant == NULL);
    detruire_tournoi(t);

    t = tous_les_tours(preparer_tournoi(creer_liste_2()));
    assert(t->vainqueur == 7);
    assert(tete_liste(t->vaincus) == 2);
    queue_liste(t->vaincus);
    assert(est_vide_liste(t->vaincus));
    assert(t->suivant == NULL);
    detruire_tournoi(t);

    t = tous_les_tours(preparer_tournoi(creer_liste_3()));
    assert(t->vainqueur == 6);
    assert(t->suivant == NULL);
    detruire_tournoi(t);

    t = tous_les_tours(preparer_tournoi(creer_liste_4(34)));
    assert(t->vainqueur == 34);
    assert(t->suivant == NULL);
    detruire_tournoi(t);

    t = tous_les_tours(preparer_tournoi(creer_liste_4(1000)));
    assert(t->vainqueur == 1000);
    assert(t->suivant == NULL);
    detruire_tournoi(t);
}

int main()
{
    test_resolution_simple();
    test_partage();
    test_diviser_pour_regner();
    test_preparer_tournoi();
    test_detruire_tournoi();
    test_partage_tournoi();
    test_un_tour();
    test_tous_les_tours();
    test_tournoi();
}
